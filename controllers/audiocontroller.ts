export class AudioController {
    bgMusic;
    flipSound;
    matchSound;
    victorySound;
    gameOverSound;
    constructor() {
        this.bgMusic = new Audio('assets/Audio/creepy.mp3')
        this.flipSound = new Audio('assets/Audio/flip.wav')
        this.matchSound = new Audio('assets/Audio/match.wav')
        this.victorySound = new Audio('assets/Audio/victory.wav')
        this.gameOverSound = new Audio('assets/Audio/gameOver.wav')
        this.bgMusic.volume = 0.5
        this.bgMusic.loop = true
    }
    startMusic() {
        this.bgMusic.play()
    }
    stopMusic() {
        this.bgMusic.pause();//there is no stop music funtion in javascript
        this.bgMusic.currentTime = 0;//to restart music from 0 when you play it again
    }
    flip() {
        this.flipSound.play()
    }
    match() {
        this.matchSound.play()
    }
    victory() {
        this.stopMusic()//stop bg music for victory
        this.victorySound.play()
    }
    gameOver() {
        this.stopMusic()//stop bg music for losing
        this.gameOverSound.play()
    }
}