//////////////////////////////////////////////////////////////////////AUDIO FOR GAME//////////////////////////////////////////////////////////////////////
import { AudioController } from './audiocontroller'





export class MixOrMatch {
    cardsArray;
    totalTime;
    timeRemaining;
    timer;
    ticker;
    audioController;
    cardToCheck: any;
    totalClicks: any;
    matchedCards: any;
    busy:boolean;
    countDown: any;
    constructor(totalTime: any, cards:any){
        this.cardsArray = cards
        this.totalTime = totalTime
        this.timeRemaining = totalTime
        this.timer = document.getElementById('time-remaining')
        this.ticker =  document.getElementById('Flips')
        this.audioController = new AudioController()
    }
    startGame(){
        this.cardToCheck = null//when a card has been clicked and is being checked to match another card it's not null
        this.totalClicks = 0
        this.timeRemaining = this.totalTime
        this.matchedCards = []
        //animation or something is happening --- you're not allowed to click until it's done.
        this.busy = true
        //wait 500ms before doing function
        setTimeout(() => {
            this.audioController.startMusic()
            this.shuffleCards();
            this.countDown = this.startCountDown()
            this.busy = false
        }, 500)
        this.hideCards();
        this.timer.innerText = this.timeRemaining
        this.ticker.innerText = this.totalClicks
    }
    startCountDown(){
        return setInterval(() => {
            this.timeRemaining --;
            this.timer.innerText = this.timeRemaining;
            if(this.timeRemaining === 0){
                this.gameOver()
            }
        }, 1000)
    }
    hideCards(){
        this.cardsArray.forEach((card: any) => {
            card.classList.remove('visible')
            card.classList.remove('matched')
        })
    }
    flipCard(card:any){
        if(this.cardFlipCard(card)){
            this.audioController.flip()
            this.totalClicks++
            this.ticker.innerText = this.totalClicks
            card.classList.add('visible')

            if(this.cardToCheck){
                this.checkForCardMatch(card)
            }else {
                this.cardToCheck = card
            }
        }
    }
    checkForCardMatch(card:any) {
        if(this.getCardType(card) === this.getCardType(this.cardToCheck)){
           this.cardMatch(card, this.cardToCheck)
        }else {
            this.cardMisMatch(card, this.cardToCheck)
        }
        this.cardToCheck = null
    }
    cardMatch(card1:any, card2:any){
        this.matchedCards.push(card1)
        this.matchedCards.push(card2)
        card1.classList.add('matched')
        card2.classList.add('matched')
        this.audioController.match()
        if(this.matchedCards.length === this.cardsArray.length){
            this.victory()
        }
    }
    cardMisMatch(card1:any, card2:any){
        this.busy = true
        setTimeout(() =>{
            card1.classList.remove('visible')
            card2.classList.remove('visible')
            this.busy = false
        },1000)
    }
    getCardType(card:any){
        return card.getElementsByClassName('card-value')[0].src
    }
    //Fisher Yates shuffle
    shuffleCards(){
        for(let i = this.cardsArray.length - 1; i > 0; i--){
            //math.random creates random float between 0 and 1 but not 1
            let randomIndex = Math.floor(Math.random() * (i + 1))
            //since we are using css grid that has order, we are not shuffling the cards themselves but the order they show up on the grid
            this.cardsArray[randomIndex].style.order = i
            this.cardsArray[i].style.order = randomIndex
        }
    }
    cardFlipCard(card: any){
        //If card is not busy
        //If card is not matched
        //if a card is not currently being checked
        return(!this.busy && !this.matchedCards.includes(card) && card !== this.cardToCheck)
    }
    gameOver(){
        clearInterval(this.countDown);
        this.audioController.gameOver()
        document.getElementById('game-over-text').classList.add('visible')
        this.hideCards()
    }
    victory(){
        clearInterval(this.countDown);
        this.audioController.victory()
        document.getElementById('victory-text').classList.add('visible')
        this.hideCards()
    }
}