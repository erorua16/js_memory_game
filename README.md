# TS MEMORY GAME
## DESCRIPTION
This is a simple memory game made with typescript, html, and sass. 
1. To start the game, click anywhere on the screen. A timer and a counter of the number of times the user has flipped a card will run.
2. Click a card, it will flip it
3. Click another card, it will also flip it
4. If the cards are a match, the cards will stay right-side up, otherwise the cards will flip back down after a short-window of time to let the user see them.
5. Once all the cards have been matched, a victory screen will appear and the cards will flip back down. If you wish to play again simply click anywhere on the screen
6. If the timer runs out, the game over screen will appear and the cards will flip back down. To play again, simply click anywhere on the screen
## Further Development
1. Run `npm install` to get the necessary packages 
2. To compile css, use `npm run comp`
3. To compile typescript, use `npm run prestart`
