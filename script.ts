/////////////////////////////////////////////////////////////////////////////////GAME FUNCTIONS//////////////////////////////////////////////////////

import { MixOrMatch } from '../controllers/mixandmatch'


/////////////////////////////////////////////////////////////////////////////////LOAD JAVASCRIPT DOCUMENT BEFORE HTML/////////////////////////////////////////////////////////////////////////////////


function ready() {
    //returns an html collection, it cant access javascript array functions so you use Array.from
    var overlays = Array.from(document.getElementsByClassName('overlay-text'))
    var cards = Array.from(document.getElementsByClassName('card'))
    var game = new MixOrMatch(100, cards)


    overlays.forEach(overlay =>{
        overlay.addEventListener('click', () => {
            overlay.classList.remove('visible');
            game.startGame()
        })
    })
    cards.forEach(card => {
        card.addEventListener('click', () => {
            game.flipCard(card)
        })
    })
}
    
if(document.readyState ==='loading') {  
    //Once everything in html is loaded, it will call ready()
    document.addEventListener('DOMContentLoaded', (event: Event) => ready())
}else {
    ready()
}