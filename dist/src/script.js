"use strict";
/////////////////////////////////////////////////////////////////////////////////GAME FUNCTIONS//////////////////////////////////////////////////////
Object.defineProperty(exports, "__esModule", { value: true });
const mixandmatch_1 = require("../controllers/mixandmatch");
/////////////////////////////////////////////////////////////////////////////////LOAD JAVASCRIPT DOCUMENT BEFORE HTML/////////////////////////////////////////////////////////////////////////////////
function ready() {
    //returns an html collection, it cant access javascript array functions so you use Array.from
    var overlays = Array.from(document.getElementsByClassName('overlay-text'));
    var cards = Array.from(document.getElementsByClassName('card'));
    var game = new mixandmatch_1.MixOrMatch(100, cards);
    overlays.forEach(overlay => {
        overlay.addEventListener('click', () => {
            overlay.classList.remove('visible');
            game.startGame();
        });
    });
    cards.forEach(card => {
        card.addEventListener('click', () => {
            game.flipCard(card);
        });
    });
}
if (document.readyState === 'loading') {
    //Once everything in html is loaded, it will call ready()
    document.addEventListener('DOMContentLoaded', (event) => ready());
}
else {
    ready();
}
//# sourceMappingURL=script.js.map